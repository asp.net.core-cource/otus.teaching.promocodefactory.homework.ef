﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromocodesController(IRepository<PromoCode> promocodeRepository, 
            IRepository<Preference> preferenceRepository,
            IRepository<Employee> employeeRepository,
            IRepository<Customer> customersRepository)
        {
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
            _employeeRepository = employeeRepository;
            _customersRepository = customersRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var preferences = await _promocodeRepository.GetAllAsync();

            var preferencesModelList = preferences.Select(x =>
                new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName
                }).ToList();

            return preferencesModelList;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var employees = await _employeeRepository.GetAllAsync();
            var employee = employees.FirstOrDefault(e => e.FullName == request.PartnerName);

            if(employee == null)
                return NotFound();

            var preferences = await _preferenceRepository.GetAllAsync();
            var preference = preferences.FirstOrDefault(e => e.Name == request.Preference);

            if(preference == null)
                return NotFound();

            var customers = await _customersRepository.GetAllAsync();
            var customer = customers.FirstOrDefault(c => c.CustomerPreferences.Select(p => p.PreferenceId).Contains(preference.Id));

            if(customer == null)
                return BadRequest();

            var promocode = new PromoCode
            {
                Id = Guid.NewGuid(),
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(30),
                PartnerName = request.PartnerName,
                Preference = new Preference { Name = request.Preference },
                Customer = customer
            };

            await _promocodeRepository.AddEntityAsync(promocode);

            return new OkResult();
        }
    }
}