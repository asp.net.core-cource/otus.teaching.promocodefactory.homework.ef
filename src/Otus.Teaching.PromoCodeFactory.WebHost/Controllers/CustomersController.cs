﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customersRepository;

        public CustomersController(IRepository<Customer> customersRepository)
        {
            _customersRepository = customersRepository;
        }

        /// <summary>
        /// Получить данные о всех клиентах.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customersRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email

                }).ToList();

            return customersModelList;
        }
        
        /// <summary>
        /// Получить данные о клиенте по id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);

            if(customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.CustomerPreferences.Select(p => new PreferenceResponse
                {
                    Name = p.Preference.Name
                }).ToList(),
                PromoCodes = customer.Promocodes.Select(p => new PromoCodeShortResponse
				{
                    Id = p.Id,
                    Code = p.Code,
                    ServiceInfo = p.ServiceInfo,
                    BeginDate = p.BeginDate.ToString(),
                    EndDate = p.EndDate.ToString(),
                    PartnerName = p.PartnerName
                }).ToList()
            };

            return customerModel;
        }
        
        /// <summary>
        /// Создать нового клиента.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer 
            { 
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreferences = request.PreferenceIds.Select(id => 
                    new CustomerPreference 
                    {
                       PreferenceId = id,
                       Preference = new Preference { Id = id }
                    }).ToList()
            };

            await _customersRepository.AddEntityAsync(customer);

            return new OkResult();
        }
        
        /// <summary>
        /// Обновить данные клиента.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customersRepository.GetByIdAsync(id);

            if(customer == null)
                return NotFound();

            var customerToUpdate = new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreferences = request.PreferenceIds.Select(id =>
                    new CustomerPreference
                    {
                        PreferenceId = id,
                        Preference = new Preference { Id = id }
                    }).ToList()
            };

            _customersRepository.UpdateEntityAsync(customerToUpdate);

            return new OkResult();
        }
        
        /// <summary>
        /// Удалить клиента.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);

            if(customer == null)
                return NotFound();

            _customersRepository.DeleteEntity(customer);

            return new OkResult();
        }
    }
}