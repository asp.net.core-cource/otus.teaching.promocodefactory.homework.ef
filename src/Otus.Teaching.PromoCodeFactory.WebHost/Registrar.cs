﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Settings;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
	public static class Registrar
	{
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var applicationSettings = configuration.Get<ApplicationSettings>();
            services.AddSingleton(applicationSettings);
            return services.AddSingleton((IConfigurationRoot)configuration)
                .ConfigureContext(applicationSettings.ConnectionString)
                .InstallRepositories();
        }

        public static IServiceCollection ConfigureContext(this IServiceCollection services,
         string connectionString)
        {
            services.AddDbContext<DatabaseContext>(optionsBuilder
                => optionsBuilder
                    .UseSqlite(connectionString));
            return services;
        }
        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
            .AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            return serviceCollection;
        }
    }
}
