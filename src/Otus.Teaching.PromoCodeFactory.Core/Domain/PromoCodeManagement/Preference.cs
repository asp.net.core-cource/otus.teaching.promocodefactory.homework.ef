﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [MaxLength(60)]
        public string Name { get; set; }

        public IList<CustomerPreference> CustomerPreferences { get; set; }

        public Guid PreferenceOfPromocodeId { get; set; }
        public PromoCode Promocode { get; set; }
    }
}