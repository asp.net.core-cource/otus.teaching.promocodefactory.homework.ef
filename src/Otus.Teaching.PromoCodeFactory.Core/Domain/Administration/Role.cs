﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [MaxLength(60)]
        public string Name { get; set; }

        //[MaxLength(255)]
        //public string Description { get; set; }

        public Guid RoleOfEmployeeId { get; set; }
        public Employee Employee { get; set; }
    }
}