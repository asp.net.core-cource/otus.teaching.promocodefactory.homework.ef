﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        private static Guid _promocodeId1 = Guid.NewGuid();
        private static Guid _promocodeId2 = Guid.NewGuid();
        private static Guid _promocodeId3 = Guid.NewGuid();

        private static Guid _employeeId1 = Guid.NewGuid();
        private static Guid _employeeId2 = Guid.NewGuid();
        private static Guid _employeeId3 = Guid.NewGuid();

        private static Guid _preferenceId1 = Guid.NewGuid();
        private static Guid _preferenceId2 = Guid.NewGuid();
        private static Guid _preferenceId3 = Guid.NewGuid();

        private static Guid _customerId1 = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
        private static Guid _customerId2 = Guid.NewGuid();
        private static Guid _customerId3 = Guid.NewGuid();
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = _employeeId1,
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                EmployeeOfPromocodeId = _promocodeId1,
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = _employeeId2,
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                EmployeeOfPromocodeId = _promocodeId2,
                AppliedPromocodesCount = 10
            },
            new Employee()
            {
                Id = _employeeId3,
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                EmployeeOfPromocodeId = _promocodeId3,
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                //Description = "Администратор",
                RoleOfEmployeeId = _employeeId1
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
               //Description = "Партнерский менеджер",
                RoleOfEmployeeId = _employeeId2
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = _preferenceId1,
                Name = "Театр",
                PreferenceOfPromocodeId = _promocodeId1
            },
			new Preference()
			{
				Id = _preferenceId2,
				Name = "Семья",
				PreferenceOfPromocodeId = _promocodeId2
			},
			new Preference()
			{
				Id = _preferenceId3,
				Name = "Дети",
				PreferenceOfPromocodeId = _promocodeId3
			}
		};

        public static IEnumerable<PromoCode> Promocodes => new List<PromoCode>()
        {
            new PromoCode()
            {
                Id = _promocodeId1,
                Code = "Code1",
                ServiceInfo = "ServiceInfo1",
                BeginDate = DateTime.Parse("2022-05-01T07:34:42-5:00"),
                EndDate = DateTime.Parse("2035-05-01T07:34:42-5:00"),
                PartnerName = "PartnerName1",
                CurrentCustomerId = _customerId1
            },
             new PromoCode()
            {
                Id = _promocodeId2,
                Code = "Code2",
                ServiceInfo = "ServiceInfo2",
                BeginDate = DateTime.Parse("2022-05-01T07:34:42-5:00"),
                EndDate = DateTime.Parse("2035-05-01T07:34:42-5:00"),
                PartnerName = "PartnerName2",
                CurrentCustomerId = _customerId1
            },
              new PromoCode()
            {
                Id = _promocodeId3,
                Code = "Code3",
                ServiceInfo = "ServiceInfo3",
                BeginDate = DateTime.Parse("2022-05-01T07:34:42-5:00"),
                EndDate = DateTime.Parse("2035-05-01T07:34:42-5:00"),
                PartnerName = "PartnerName3",
                CurrentCustomerId = _customerId1
            },

        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = _customerId1,
                        Email = "ivan_ivanov@mail.ru",
                        FirstName = "Иван",
                        LastName = "Иванов",
                    } ,
                     new Customer()
                    {
                        Id = _customerId2,
                        Email = "ivan_petrov@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                    },
                      new Customer()
                    {
                        Id = _customerId3,
                        Email = "ivan_sidorov@mail.ru",
                        FirstName = "Иван",
                        LastName = "Сидоров",
                    }
                };

                return customers;
            }
        }

        public static IEnumerable<CustomerPreference> CustomerPreferences => new List<CustomerPreference> 
        {
            new CustomerPreference
            {
                CustomerId = _customerId1,
                PreferenceId = _preferenceId1
            },
            new CustomerPreference
            {
                CustomerId = _customerId2,
                PreferenceId = _preferenceId2
            },
            new CustomerPreference
            {
                CustomerId = _customerId3,
                PreferenceId = _preferenceId3
            }
        };
    }
}