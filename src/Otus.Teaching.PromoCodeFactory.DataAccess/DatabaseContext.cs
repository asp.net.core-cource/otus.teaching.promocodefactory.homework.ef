﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
	public class DatabaseContext : DbContext
	{
		public DbSet<Employee> Employees { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<PromoCode> Promocodes { get; set; }
		public DbSet<Preference> Preferences { get; set; }
		public DbSet<CustomerPreference> CustomerPreferences { get; set; }

		public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
		{
			Database.EnsureDeleted();
			Database.EnsureCreated();
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<CustomerPreference>().HasKey(sc => new { sc.CustomerId, sc.PreferenceId });

			modelBuilder.Entity<CustomerPreference>()
				.HasOne<Customer>(sc => sc.Customer)
				.WithMany(s => s.CustomerPreferences)
				.HasForeignKey(sc => sc.CustomerId);

			modelBuilder.Entity<CustomerPreference>()
				.HasOne<Preference>(sc => sc.Preference)
				.WithMany(s => s.CustomerPreferences)
				.HasForeignKey(sc => sc.PreferenceId);

			modelBuilder.Entity<PromoCode>()
				.HasOne<Customer>(s => s.Customer)
				.WithMany(g => g.Promocodes)
				.HasForeignKey(s => s.CurrentCustomerId);

			modelBuilder.Entity<Employee>()
				.HasOne<Role>(s => s.Role)
				.WithOne(ad => ad.Employee)
				.HasForeignKey<Role>(ad => ad.RoleOfEmployeeId);

			modelBuilder.Entity<PromoCode>()
				.HasOne<Preference>(s => s.Preference)
				.WithOne(ad => ad.Promocode)
				.HasForeignKey<Preference>(ad => ad.PreferenceOfPromocodeId);

			modelBuilder.Entity<PromoCode>()
				.HasOne<Employee>(s => s.PartnerManager)
				.WithOne(ad => ad.Promocode)
				.HasForeignKey<Employee>(ad => ad.EmployeeOfPromocodeId);

			modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
			modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
			modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
			modelBuilder.Entity<PromoCode>().HasData(FakeDataFactory.Promocodes);
			modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
			modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);
 		}
	}

}
