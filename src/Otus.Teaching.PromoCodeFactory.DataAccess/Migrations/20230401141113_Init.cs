﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Promocodes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    ServiceInfo = table.Column<string>(nullable: true),
                    BeginDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    PartnerName = table.Column<string>(nullable: true),
                    CurrentCustomerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Promocodes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Promocodes_Customers_CurrentCustomerId",
                        column: x => x.CurrentCustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    AppliedPromocodesCount = table.Column<int>(nullable: false),
                    EmployeeOfPromocodeId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employees_Promocodes_EmployeeOfPromocodeId",
                        column: x => x.EmployeeOfPromocodeId,
                        principalTable: "Promocodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Preferences",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    PreferenceOfPromocodeId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Preferences", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Preferences_Promocodes_PreferenceOfPromocodeId",
                        column: x => x.PreferenceOfPromocodeId,
                        principalTable: "Promocodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    RoleOfEmployeeId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Roles_Employees_RoleOfEmployeeId",
                        column: x => x.RoleOfEmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CustomerPreferences",
                columns: table => new
                {
                    CustomerId = table.Column<Guid>(nullable: false),
                    PreferenceId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerPreferences", x => new { x.CustomerId, x.PreferenceId });
                    table.ForeignKey(
                        name: "FK_CustomerPreferences_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerPreferences_Preferences_PreferenceId",
                        column: x => x.PreferenceId,
                        principalTable: "Preferences",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), "ivan_ivanov@mail.ru", "Иван", "Иванов" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("9b6314f1-ac41-47f0-bc41-383c55f13cb0"), "ivan_petrov@mail.ru", "Иван", "Петров" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("99c11c29-4d67-42e3-9fb8-8c834b0074b9"), "ivan_sidorov@mail.ru", "Иван", "Сидоров" });

            migrationBuilder.InsertData(
                table: "Promocodes",
                columns: new[] { "Id", "BeginDate", "Code", "CurrentCustomerId", "EndDate", "PartnerName", "ServiceInfo" },
                values: new object[] { new Guid("297d8bf6-bd59-425d-a0f2-6ed13ee2d8bb"), new DateTime(2022, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "Code1", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new DateTime(2035, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "PartnerName1", "ServiceInfo1" });

            migrationBuilder.InsertData(
                table: "Promocodes",
                columns: new[] { "Id", "BeginDate", "Code", "CurrentCustomerId", "EndDate", "PartnerName", "ServiceInfo" },
                values: new object[] { new Guid("161a50ee-1d92-467f-9e30-91a6d8fa7a89"), new DateTime(2022, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "Code2", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new DateTime(2035, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "PartnerName2", "ServiceInfo2" });

            migrationBuilder.InsertData(
                table: "Promocodes",
                columns: new[] { "Id", "BeginDate", "Code", "CurrentCustomerId", "EndDate", "PartnerName", "ServiceInfo" },
                values: new object[] { new Guid("99645e48-f38d-43a2-9017-c5be4f89b3fb"), new DateTime(2022, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "Code3", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new DateTime(2035, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "PartnerName3", "ServiceInfo3" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "EmployeeOfPromocodeId", "FirstName", "LastName" },
                values: new object[] { new Guid("6d22e3d3-d006-41c0-9264-ae377de1fcda"), 5, "owner@somemail.ru", new Guid("297d8bf6-bd59-425d-a0f2-6ed13ee2d8bb"), "Иван", "Сергеев" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "EmployeeOfPromocodeId", "FirstName", "LastName" },
                values: new object[] { new Guid("12623d06-84ab-4df2-b7fb-6d5d00d185e9"), 10, "andreev@somemail.ru", new Guid("161a50ee-1d92-467f-9e30-91a6d8fa7a89"), "Петр", "Андреев" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "EmployeeOfPromocodeId", "FirstName", "LastName" },
                values: new object[] { new Guid("3cd51471-3c05-4cbb-ad5c-c847a9e395ea"), 10, "andreev@somemail.ru", new Guid("99645e48-f38d-43a2-9017-c5be4f89b3fb"), "Петр", "Андреев" });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "Name", "PreferenceOfPromocodeId" },
                values: new object[] { new Guid("00a56946-9f55-4102-b435-641347685bac"), "Театр", new Guid("297d8bf6-bd59-425d-a0f2-6ed13ee2d8bb") });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "Name", "PreferenceOfPromocodeId" },
                values: new object[] { new Guid("0cc79168-d082-44c3-831a-36cff2038df0"), "Семья", new Guid("161a50ee-1d92-467f-9e30-91a6d8fa7a89") });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "Name", "PreferenceOfPromocodeId" },
                values: new object[] { new Guid("7471c92c-8391-4ec8-9abe-5eadf5a96dc2"), "Дети", new Guid("99645e48-f38d-43a2-9017-c5be4f89b3fb") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("00a56946-9f55-4102-b435-641347685bac") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("9b6314f1-ac41-47f0-bc41-383c55f13cb0"), new Guid("0cc79168-d082-44c3-831a-36cff2038df0") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("99c11c29-4d67-42e3-9fb8-8c834b0074b9"), new Guid("7471c92c-8391-4ec8-9abe-5eadf5a96dc2") });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Description", "Name", "RoleOfEmployeeId" },
                values: new object[] { new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), "Администратор", "Admin", new Guid("6d22e3d3-d006-41c0-9264-ae377de1fcda") });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Description", "Name", "RoleOfEmployeeId" },
                values: new object[] { new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665"), "Партнерский менеджер", "PartnerManager", new Guid("12623d06-84ab-4df2-b7fb-6d5d00d185e9") });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPreferences_PreferenceId",
                table: "CustomerPreferences",
                column: "PreferenceId");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_EmployeeOfPromocodeId",
                table: "Employees",
                column: "EmployeeOfPromocodeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Preferences_PreferenceOfPromocodeId",
                table: "Preferences",
                column: "PreferenceOfPromocodeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Promocodes_CurrentCustomerId",
                table: "Promocodes",
                column: "CurrentCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_RoleOfEmployeeId",
                table: "Roles",
                column: "RoleOfEmployeeId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerPreferences");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Preferences");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Promocodes");

            migrationBuilder.DropTable(
                name: "Customers");
        }
    }
}
