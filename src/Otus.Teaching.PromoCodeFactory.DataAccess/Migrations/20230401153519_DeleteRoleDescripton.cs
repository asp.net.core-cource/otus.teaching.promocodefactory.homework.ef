﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class DeleteRoleDescripton : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumns: new[] { "CustomerId", "PreferenceId" },
                keyValues: new object[] { new Guid("99c11c29-4d67-42e3-9fb8-8c834b0074b9"), new Guid("7471c92c-8391-4ec8-9abe-5eadf5a96dc2") });

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumns: new[] { "CustomerId", "PreferenceId" },
                keyValues: new object[] { new Guid("9b6314f1-ac41-47f0-bc41-383c55f13cb0"), new Guid("0cc79168-d082-44c3-831a-36cff2038df0") });

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumns: new[] { "CustomerId", "PreferenceId" },
                keyValues: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("00a56946-9f55-4102-b435-641347685bac") });

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: new Guid("3cd51471-3c05-4cbb-ad5c-c847a9e395ea"));

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"));

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("99c11c29-4d67-42e3-9fb8-8c834b0074b9"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("9b6314f1-ac41-47f0-bc41-383c55f13cb0"));

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: new Guid("12623d06-84ab-4df2-b7fb-6d5d00d185e9"));

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: new Guid("6d22e3d3-d006-41c0-9264-ae377de1fcda"));

            migrationBuilder.DeleteData(
                table: "Preferences",
                keyColumn: "Id",
                keyValue: new Guid("00a56946-9f55-4102-b435-641347685bac"));

            migrationBuilder.DeleteData(
                table: "Preferences",
                keyColumn: "Id",
                keyValue: new Guid("0cc79168-d082-44c3-831a-36cff2038df0"));

            migrationBuilder.DeleteData(
                table: "Preferences",
                keyColumn: "Id",
                keyValue: new Guid("7471c92c-8391-4ec8-9abe-5eadf5a96dc2"));

            migrationBuilder.DeleteData(
                table: "Promocodes",
                keyColumn: "Id",
                keyValue: new Guid("161a50ee-1d92-467f-9e30-91a6d8fa7a89"));

            migrationBuilder.DeleteData(
                table: "Promocodes",
                keyColumn: "Id",
                keyValue: new Guid("297d8bf6-bd59-425d-a0f2-6ed13ee2d8bb"));

            migrationBuilder.DeleteData(
                table: "Promocodes",
                keyColumn: "Id",
                keyValue: new Guid("99645e48-f38d-43a2-9017-c5be4f89b3fb"));

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Roles");

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("b782b026-f13d-4c9e-a601-132252ed4497"), "ivan_petrov@mail.ru", "Иван", "Петров" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("3b6fe240-2695-4cb2-b63d-135980f8d8f4"), "ivan_sidorov@mail.ru", "Иван", "Сидоров" });

            migrationBuilder.InsertData(
                table: "Promocodes",
                columns: new[] { "Id", "BeginDate", "Code", "CurrentCustomerId", "EndDate", "PartnerName", "ServiceInfo" },
                values: new object[] { new Guid("6f4953c1-c6b8-4dcc-bfcf-50c8c5a1c4ae"), new DateTime(2022, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "Code1", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new DateTime(2035, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "PartnerName1", "ServiceInfo1" });

            migrationBuilder.InsertData(
                table: "Promocodes",
                columns: new[] { "Id", "BeginDate", "Code", "CurrentCustomerId", "EndDate", "PartnerName", "ServiceInfo" },
                values: new object[] { new Guid("733d3c4a-8419-4355-a127-a9bf10b46e7a"), new DateTime(2022, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "Code2", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new DateTime(2035, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "PartnerName2", "ServiceInfo2" });

            migrationBuilder.InsertData(
                table: "Promocodes",
                columns: new[] { "Id", "BeginDate", "Code", "CurrentCustomerId", "EndDate", "PartnerName", "ServiceInfo" },
                values: new object[] { new Guid("edeedf21-ffe3-4f72-90b0-c1dca969f673"), new DateTime(2022, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "Code3", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new DateTime(2035, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "PartnerName3", "ServiceInfo3" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "EmployeeOfPromocodeId", "FirstName", "LastName" },
                values: new object[] { new Guid("fde35f0b-3c10-4520-a489-a45841b7c96f"), 5, "owner@somemail.ru", new Guid("6f4953c1-c6b8-4dcc-bfcf-50c8c5a1c4ae"), "Иван", "Сергеев" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "EmployeeOfPromocodeId", "FirstName", "LastName" },
                values: new object[] { new Guid("7650bdaf-875f-4b58-a6cf-e6a9fd531435"), 10, "andreev@somemail.ru", new Guid("733d3c4a-8419-4355-a127-a9bf10b46e7a"), "Петр", "Андреев" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "EmployeeOfPromocodeId", "FirstName", "LastName" },
                values: new object[] { new Guid("2cd26ca8-91fc-439a-a234-c28c5f4c455a"), 10, "andreev@somemail.ru", new Guid("edeedf21-ffe3-4f72-90b0-c1dca969f673"), "Петр", "Андреев" });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "Name", "PreferenceOfPromocodeId" },
                values: new object[] { new Guid("11be81a8-242b-43da-a2b4-b638f939e819"), "Театр", new Guid("6f4953c1-c6b8-4dcc-bfcf-50c8c5a1c4ae") });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "Name", "PreferenceOfPromocodeId" },
                values: new object[] { new Guid("41e47e28-9e63-4f26-b638-4df9020046b8"), "Семья", new Guid("733d3c4a-8419-4355-a127-a9bf10b46e7a") });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "Name", "PreferenceOfPromocodeId" },
                values: new object[] { new Guid("5e851ab7-a678-468a-a80f-eb63e12ddef9"), "Дети", new Guid("edeedf21-ffe3-4f72-90b0-c1dca969f673") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("11be81a8-242b-43da-a2b4-b638f939e819") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("b782b026-f13d-4c9e-a601-132252ed4497"), new Guid("41e47e28-9e63-4f26-b638-4df9020046b8") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("3b6fe240-2695-4cb2-b63d-135980f8d8f4"), new Guid("5e851ab7-a678-468a-a80f-eb63e12ddef9") });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name", "RoleOfEmployeeId" },
                values: new object[] { new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), "Admin", new Guid("fde35f0b-3c10-4520-a489-a45841b7c96f") });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name", "RoleOfEmployeeId" },
                values: new object[] { new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665"), "PartnerManager", new Guid("7650bdaf-875f-4b58-a6cf-e6a9fd531435") });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumns: new[] { "CustomerId", "PreferenceId" },
                keyValues: new object[] { new Guid("3b6fe240-2695-4cb2-b63d-135980f8d8f4"), new Guid("5e851ab7-a678-468a-a80f-eb63e12ddef9") });

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumns: new[] { "CustomerId", "PreferenceId" },
                keyValues: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("11be81a8-242b-43da-a2b4-b638f939e819") });

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumns: new[] { "CustomerId", "PreferenceId" },
                keyValues: new object[] { new Guid("b782b026-f13d-4c9e-a601-132252ed4497"), new Guid("41e47e28-9e63-4f26-b638-4df9020046b8") });

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: new Guid("2cd26ca8-91fc-439a-a234-c28c5f4c455a"));

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"));

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("3b6fe240-2695-4cb2-b63d-135980f8d8f4"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("b782b026-f13d-4c9e-a601-132252ed4497"));

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: new Guid("7650bdaf-875f-4b58-a6cf-e6a9fd531435"));

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: new Guid("fde35f0b-3c10-4520-a489-a45841b7c96f"));

            migrationBuilder.DeleteData(
                table: "Preferences",
                keyColumn: "Id",
                keyValue: new Guid("11be81a8-242b-43da-a2b4-b638f939e819"));

            migrationBuilder.DeleteData(
                table: "Preferences",
                keyColumn: "Id",
                keyValue: new Guid("41e47e28-9e63-4f26-b638-4df9020046b8"));

            migrationBuilder.DeleteData(
                table: "Preferences",
                keyColumn: "Id",
                keyValue: new Guid("5e851ab7-a678-468a-a80f-eb63e12ddef9"));

            migrationBuilder.DeleteData(
                table: "Promocodes",
                keyColumn: "Id",
                keyValue: new Guid("6f4953c1-c6b8-4dcc-bfcf-50c8c5a1c4ae"));

            migrationBuilder.DeleteData(
                table: "Promocodes",
                keyColumn: "Id",
                keyValue: new Guid("733d3c4a-8419-4355-a127-a9bf10b46e7a"));

            migrationBuilder.DeleteData(
                table: "Promocodes",
                keyColumn: "Id",
                keyValue: new Guid("edeedf21-ffe3-4f72-90b0-c1dca969f673"));

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Roles",
                type: "TEXT",
                nullable: true);

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("9b6314f1-ac41-47f0-bc41-383c55f13cb0"), "ivan_petrov@mail.ru", "Иван", "Петров" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("99c11c29-4d67-42e3-9fb8-8c834b0074b9"), "ivan_sidorov@mail.ru", "Иван", "Сидоров" });

            migrationBuilder.InsertData(
                table: "Promocodes",
                columns: new[] { "Id", "BeginDate", "Code", "CurrentCustomerId", "EndDate", "PartnerName", "ServiceInfo" },
                values: new object[] { new Guid("297d8bf6-bd59-425d-a0f2-6ed13ee2d8bb"), new DateTime(2022, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "Code1", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new DateTime(2035, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "PartnerName1", "ServiceInfo1" });

            migrationBuilder.InsertData(
                table: "Promocodes",
                columns: new[] { "Id", "BeginDate", "Code", "CurrentCustomerId", "EndDate", "PartnerName", "ServiceInfo" },
                values: new object[] { new Guid("161a50ee-1d92-467f-9e30-91a6d8fa7a89"), new DateTime(2022, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "Code2", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new DateTime(2035, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "PartnerName2", "ServiceInfo2" });

            migrationBuilder.InsertData(
                table: "Promocodes",
                columns: new[] { "Id", "BeginDate", "Code", "CurrentCustomerId", "EndDate", "PartnerName", "ServiceInfo" },
                values: new object[] { new Guid("99645e48-f38d-43a2-9017-c5be4f89b3fb"), new DateTime(2022, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "Code3", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new DateTime(2035, 5, 1, 17, 34, 42, 0, DateTimeKind.Local), "PartnerName3", "ServiceInfo3" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "EmployeeOfPromocodeId", "FirstName", "LastName" },
                values: new object[] { new Guid("6d22e3d3-d006-41c0-9264-ae377de1fcda"), 5, "owner@somemail.ru", new Guid("297d8bf6-bd59-425d-a0f2-6ed13ee2d8bb"), "Иван", "Сергеев" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "EmployeeOfPromocodeId", "FirstName", "LastName" },
                values: new object[] { new Guid("12623d06-84ab-4df2-b7fb-6d5d00d185e9"), 10, "andreev@somemail.ru", new Guid("161a50ee-1d92-467f-9e30-91a6d8fa7a89"), "Петр", "Андреев" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "EmployeeOfPromocodeId", "FirstName", "LastName" },
                values: new object[] { new Guid("3cd51471-3c05-4cbb-ad5c-c847a9e395ea"), 10, "andreev@somemail.ru", new Guid("99645e48-f38d-43a2-9017-c5be4f89b3fb"), "Петр", "Андреев" });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "Name", "PreferenceOfPromocodeId" },
                values: new object[] { new Guid("00a56946-9f55-4102-b435-641347685bac"), "Театр", new Guid("297d8bf6-bd59-425d-a0f2-6ed13ee2d8bb") });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "Name", "PreferenceOfPromocodeId" },
                values: new object[] { new Guid("0cc79168-d082-44c3-831a-36cff2038df0"), "Семья", new Guid("161a50ee-1d92-467f-9e30-91a6d8fa7a89") });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "Name", "PreferenceOfPromocodeId" },
                values: new object[] { new Guid("7471c92c-8391-4ec8-9abe-5eadf5a96dc2"), "Дети", new Guid("99645e48-f38d-43a2-9017-c5be4f89b3fb") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("00a56946-9f55-4102-b435-641347685bac") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("9b6314f1-ac41-47f0-bc41-383c55f13cb0"), new Guid("0cc79168-d082-44c3-831a-36cff2038df0") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("99c11c29-4d67-42e3-9fb8-8c834b0074b9"), new Guid("7471c92c-8391-4ec8-9abe-5eadf5a96dc2") });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Description", "Name", "RoleOfEmployeeId" },
                values: new object[] { new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), "Администратор", "Admin", new Guid("6d22e3d3-d006-41c0-9264-ae377de1fcda") });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Description", "Name", "RoleOfEmployeeId" },
                values: new object[] { new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665"), "Партнерский менеджер", "PartnerManager", new Guid("12623d06-84ab-4df2-b7fb-6d5d00d185e9") });
        }
    }
}
